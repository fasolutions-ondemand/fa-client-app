/**
 * Standard Solution transaction type codes.
 */
export enum TransactionType {
  DEPOSIT = "DEP",
  WITHDRAWAL = "WD",
  BUY = "B",
  SELL = "S",
  SUBSCRIPTION = "SUB",
  REDEMPTION = "RED",
  UNDEFINED = "",
}
