import { OrderStatus, TransactionTypeAmountEffect } from "api/enums";
import { TransactionType } from "api/transactions/enums";

export interface TradeOrderType {
  typeCode: TransactionType;
  typeName: string;
  typeNamesAsMap?: Record<string, string>;
  cashFlowEffect: number;
  amountEffect: TransactionTypeAmountEffect;
}

export interface TradeOrder {
  id: number;
  amount?: number;
  securityName: string;
  security?: {
    // not returned for cash
    id: number;
    name: string;
    namesAsMap: Record<string, string>;
  };
  type: TradeOrderType;
  transactionDate: string;
  tradeAmountInPortfolioCurrency?: number;
  parentPortfolio: {
    id: number;
  };
  reference: string;
  orderStatus: OrderStatus;
  extId?: string;
  linkedTransaction: TradeOrder | null;
}

export interface TradeOrderDetails extends TradeOrder {
  security?: {
    id: number;
    name: string;
    namesAsMap: Record<string, string>;
    isinCode: string;
    country?: {
      id: number;
      code: string;
    };
    exchange?: {
      name: string;
    };
  };
  settlementDate: string;
  unitPriceInSecurityCurrency: number;
  costInSecurityCurrency: number;
  accountFxRate: number;
  documents: {
    identifier: string;
  }[];
  extInfo: string;
  marketPlace?: {
    name: string;
  };
  account?: {
    currency: {
      accountCurrencyCode: string;
    };
  };
  securityCurrencyCode: string;
  tradeAmountInAccountCurrency: number;
  tradeAmountInSecurityCurrency: number;
  grossPriceInSecurityCurrency: number;
  grossPriceInAccountCurrency: number;
  reference: string;
  linkedTransaction: TradeOrderDetails | null;
  taxType: {
    namesAsMap: Record<string, string>;
    id: number;
  } | null;
  taxType2: {
    namesAsMap: Record<string, string>;
    id: number;
  } | null;
  tax: number;
  tax2: number;
}

export interface TradeOrderDetailsQuery {
  order: TradeOrderDetails;
}

export interface AllTradeOrdersQuery {
  contact: {
    id: number;
    tradeOrders: TradeOrder[];
  };
}

export interface PortfolioTradeOrdersQuery {
  portfolios: {
    id: number;
    tradeOrders: TradeOrder[];
  }[];
}

export interface TradeOrderQueryById {
  transaction: TradeOrder;
}

export interface TradeOrderQuery {
  tradeOrders: TradeOrder[];
}

interface OrderFromMutation {
  "o.maturityDate": string;
  "o.costType2": string;
  "o.origTransactionDate": string;
  "o.marketPlace": string;
  "o.counter": string;
  "o.extId": string;
  "o.unitPrice": string;
  "o.costType1": string;
  "o.accrual": string;
  "o.compCost2": string;
  "o.compCost1": string;
  "o.ratio": string;
  "o.description": string;
  "o.amount": string;
  "o.cost": string;
  "o.accountFxRate": string;
  "o.accruedInterest": string;
  "o.parentPortfolio": string;
  "o.basis": string;
  "o.securityType": string;
  "o.account": string;
  "o.origUnitPrice": string;
  "o.settlementDate": string;
  "o.status": string;
  "o.currency": string;
  "o.fxRate": string;
  importStatus: string;
  "o.reportFxRate": string;
  "o.prefix": string;
  "o.hidden": string;
  "o.security": string;
  "o.taxType2": string;
  "o.settlementPlace": string;
  "o.security2": string;
  "o.tradeTime": string;
  "o.priority": string;
  "o.transactionDate": string;
  "o.origFxRate": string;
  "o.type": string;
  "o.counterPortfolio": string;
  "o.tradeAmount": string;
  "o.intInfo": string;
  "o.paymentDate": string;
  "o.executionMethod": string;
  "o.taxType": string;
  "o.reference": string;
  "o.cost2": string;
  "o.coefficient": string;
}

export interface OrderMutationResponse {
  importLimitedTradeOrder:
    | [Record<string, string>, OrderFromMutation] //in case of success
    | [OrderFromMutation]; //in case of error
}
