import { useMemo, useState } from "react";
import { useGetPortfolioBasicFieldsById } from "api/common/useGetPortfolioBasicFieldsById";
import { SecurityTypeCode } from "api/holdings/types";
import { PortfolioData } from "api/overview/types";
import { useGetPortfolioOverviewSmart } from "api/overview/useGetPortfolioOverviewSmart";
import { TimePeriodForGraph } from "api/performance/types";
import { useGetPerformance } from "api/performance/useGetPerformance";
import { ReactComponent as Spinner } from "assets/spinner.svg";
import classNames from "classnames";
import {
  ButtonRadio,
  Card,
  Center,
  LineChart,
  QueryLoadingWrapper,
} from "components";
import { Option } from "components/ButtonRadio/ButtonRadio";
import PieChartLazy from "components/PieChart/PieChartLazy";
import { useMatchesBreakpoint } from "hooks/useMatchesBreakpoint";
import { useModifiedTranslation } from "hooks/useModifiedTranslation";
import { useConfig } from "providers/ConfigProvider";
import { useParams } from "react-router-dom";
import { PortfolioInfoCard } from "../../overview/components/PortfolioInfoCard";
import { ListedSecuritiesCard } from "./components/ListedSecuritiesCard";
import { PortfolioSummary } from "./components/PortfolioSummary";
import { useSecuritiesSummary } from "./hooks/useSecuritiesSummary";

export const OverviewView = () => {
  const { portfolioId } = useParams();
  const portfolioIdAsNr = portfolioId ? parseInt(portfolioId, 10) : undefined;
  const analytics = useGetPortfolioOverviewSmart(portfolioIdAsNr);
  return (
    <QueryLoadingWrapper
      loading={analytics.loading}
      error={analytics.error}
      data={analytics.data}
      SuccessComponent={Overview}
    />
  );
};

interface OverviewProps {
  data: PortfolioData | undefined;
}

const Overview = ({ data }: OverviewProps) => {
  const { portfolioId } = useParams();
  const portfolioIdAsNr = portfolioId ? parseInt(portfolioId, 10) : undefined;
  const { t } = useModifiedTranslation();
  const chartRangeOptions = [
    {
      id: "DAYS-6",
      label: t("component.lineChart.rangeOptions.DAYS-6"),
    },
    {
      id: "MONTHS-1",
      label: t("component.lineChart.rangeOptions.MONTHS-1"),
    },
    {
      id: "MONTHS-3",
      label: t("component.lineChart.rangeOptions.MONTHS-3"),
    },
    {
      id: "CALYEAR-0",
      label: t("component.lineChart.rangeOptions.CALYEAR-0"),
    },
    {
      id: "CALENDYEAR-0",
      label: t("component.lineChart.rangeOptions.CALENDYEAR-0"),
      dateFormatting: {
        day: "numeric",
        month: "numeric",
        year: "2-digit",
      },
    },
  ];
  const { data: portfolioData } =
    useGetPortfolioBasicFieldsById(portfolioIdAsNr);
  const currencyCode = portfolioData?.currency?.securityCode;

  const { topSecurities, worstSecurities } = useSecuritiesSummary(
    data?.securityTypes
  );

  const [timeValue, setTimeValue] = useState<Option>(() => ({
    id: TimePeriodForGraph["DAYS-7"],
    label: t("component.lineChart.rangeOptions.DAYS-6"),
  }));

  const {
    loading,
    error,
    data: performanceChartData,
  } = useGetPerformance(Number(portfolioId), timeValue.id);

  const breakPortfolioInfoCard = useMatchesBreakpoint("md");

  const linechartData = useMemo(() => {
    if (
      performanceChartData?.dailyValue &&
      Array.isArray(performanceChartData?.dailyValue)
    ) {
      const chartData = performanceChartData.dailyValue.map((data) => ({
        x: data.date,
        y: data.indexedValue - 100,
      }));
      return chartData;
    } else {
      return [];
    }
  }, [performanceChartData]);

  const config = useConfig();

  return (
    <div className="grid grid-cols-1 md:grid-cols-2 gap-4 mb-2">
      <div className="grid md:grid-cols-3 md:col-span-2 gap-4">
        {breakPortfolioInfoCard ? (
          <PortfolioSummary
            currencyCode={currencyCode}
            accountBalance={
              data?.securityTypes?.find(
                (type) => type.code === SecurityTypeCode.CURRENCY
              )?.firstAnalysis?.marketValue
            }
            tradeAmount={data?.firstAnalysis?.tradeAmount}
            marketValue={data?.firstAnalysis?.marketValue}
          />
        ) : (
          <PortfolioInfoCard
            currentBalance={
              data?.securityTypes?.find(
                (type) => type.code === SecurityTypeCode.CURRENCY
              )?.firstAnalysis?.marketValue
            }
            currencyCode={currencyCode}
            tradeAmount={data?.firstAnalysis?.tradeAmount}
            marketValue={data?.firstAnalysis?.marketValue}
          />
        )}
      </div>
      <div className="grid gap-4">
        <ListedSecuritiesCard
          label={t("overviewPage.top3Holdings")}
          securities={topSecurities}
          currency={currencyCode}
        />
        <ListedSecuritiesCard
          label={t("overviewPage.worst3Holdings")}
          securities={worstSecurities}
          currency={currencyCode}
        />
      </div>
      <div>
        <Card header={t("overviewPage.pieChartLabel")}>
          <div className="pt-4 grow min-h-[300px]">
            <PieChartLazy
              groupBy={config?.pages?.portfolio?.overview?.piechart?.groupBy}
              groupCode={
                config?.pages?.portfolio?.overview?.piechart?.groupCode
              }
              portfolioId={portfolioId ? Number(portfolioId) : undefined}
            />
          </div>
        </Card>
      </div>
      <div>
        <Card header={t("overviewPage.lineChartLabel")}>
          <div className="pt-4 grow min-h-[300px]">
            {error ? (
              <Center>{t("messages.error")}</Center>
            ) : loading ? (
              <Center>
                <Spinner
                  className={classNames(
                    "w-5 h-5 text-primary-400 animate-spin fill-white"
                  )}
                />
              </Center>
            ) : (
              <LineChart
                series={[
                  {
                    name: t("overviewPage.lineChartTooltipLabel"),
                    data: linechartData,
                  },
                ]}
                detailed
                isPerformanceChart
              />
            )}
          </div>
          <div className="my-2.5 mx-2">
            <ButtonRadio
              value={timeValue}
              onChange={setTimeValue}
              options={chartRangeOptions}
            />
          </div>
        </Card>
      </div>
    </div>
  );
};
